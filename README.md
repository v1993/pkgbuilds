# PKGBUILDs

This repo contains PKGBUILD files for my AUR packages and is managed using [salvador](https://gitlab.com/dpeukert/salvador).

## Copyright/attribution info

### ct-honeybee man page (`ct-honeybee/ct-honeybee.1`)

Based on the [ct-honeybee README](https://github.com/SSLMate/ct-honeybee/blob/master/README) licensed under [CC0](https://github.com/SSLMate/ct-honeybee/blob/master/COPYING).

### Electron launcher scripts (`{beekeper-studio{,-ultimate},electron-*-bin,expresslrs-configurator}/electron-launcher.sh`)

Based on the [electron-launcher.sh script from the electron package in the repos](https://gitlab.archlinux.org/archlinux/packaging/packages/electron25/-/blob/main/electron-launcher.sh) presumed to be released into the public domain.

### weatherspect man pages (`weatherspect{,-git}/weatherspect.1`)

Based on the [weatherspect README](https://github.com/AnotherFoxGuy/weatherspect/blob/master/README.md) licensed under [GPLv2](https://github.com/AnotherFoxGuy/weatherspect/blob/master/gpl.txt) (changes: transformed the Markdown readme into the man page format, date: 2019/12/30), therefore licensed under [GPLv2](LICENSE.GPLv2) as well.

### All other files

Released into the public domain or where not possible, licensed under the [Unlicense](LICENSE.UNLICENSE).
