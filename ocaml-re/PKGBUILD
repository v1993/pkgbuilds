# Contributor: Jakob Gahde <j5lx@fmail.co.uk>
# Contributor: Andy Weidenbaum <archbaum@gmail.com>
# Contributor: Taylor Venable <taylor@metasyntax.net>
_projectname='re'
pkgname="ocaml-$_projectname"
pkgver='1.11.0'
pkgrel='1'
pkgdesc='Pure OCaml regular expressions, with support for Perl and POSIX-style strings'
arch=('x86_64' 'aarch64')
url="https://github.com/ocaml/$pkgname"
license=('custom:LGPL2.1 with linking exception')
depends=('ocaml>=4.03.0' 'ocaml-seq')
makedepends=('dune>=2.0.0')
checkdepends=('ocaml-ounit')
options=('!strip')
source=(
	"$pkgname-$pkgver.tar.gz::$url/archive/$pkgver.tar.gz"
	'fix-ounit-name.diff'
)
sha512sums=(
	'0000000000000000000000000000000000000000000000000000000000000000'
	'0000000000000000000000000000000000000000000000000000000000000000'
)

_sourcedirectory="$pkgname-$pkgver"

prepare() {
	cd "$srcdir/$_sourcedirectory/"
	patch --forward -p1 < '../fix-ounit-name.diff'
}

build() {
	cd "$srcdir/$_sourcedirectory/"
	dune build --release --verbose
}

check() {
	cd "$srcdir/$_sourcedirectory/"
	dune runtest --release --verbose
}

package() {
	cd "$srcdir/$_sourcedirectory/"
	DESTDIR="$pkgdir" dune install --prefix '/usr' --libdir '/usr/lib/ocaml' --docdir '/usr/share/doc' --mandir '/usr/share/man' --release --verbose

	install -dm755 "$pkgdir/usr/share/licenses/$pkgname"
	ln -sf "/usr/share/doc/$pkgname/LICENSE.md" "$pkgdir/usr/share/licenses/$pkgname/LICENSE.md"
}
